import java.util.Random;

/**
 * Class that creates a maze and sets up its method and attributes.
 * 
 * @author Alec Bokowski
 *
 */
public class Maze {
	
	
	private char[][] map;
	
	private int entranceRow;
	private int exitRow;
			
	/**
	 * Constructor for maze class that creates a random maze.
	 */
	public Maze() {
		
		//generate the random numbers for cols and rows and start and end points
		Random rand = new Random();
		int low = 6;
		int high = 12;
		int col = rand.nextInt(high-low) + low;
		int row = rand.nextInt(high-low) + low;
		//make sure start and end are logical start and end points
		int start = rand.nextInt((row - 1) - 1) + 1;
		int end = rand.nextInt((row - 1) - 1) + 1;
		map = new char[row][col];
		
		for (int r = 0; r < map.length; r++) {
			for (int c = 0; c < map[r].length; c++) {
				//do the inner ones first
				int highest = 3;
				int lowest = 1;
				int randomize = rand.nextInt(highest - lowest ) + lowest;
				
				if (randomize == 1) {
					setCell(r,c,'#');
				} else {
					setCell(r,c,'.');
				}

				//set first column to # 
				if (c == 0) {
					setCell(r,c,'#');
				}
				//set the last column to #
				if (c == map[r].length-1) {
					setCell(r,c,'#');
				}
				//set the first row to #
				if (r == 0) {
					setCell(r,c,'#');
				}
				//set the last row to #
				if (r == map.length-1) {
					setCell(r,c,'#');
				}
			}
		}
		
		//set the start and end points
		setCell(start,0,'.');
		setCell(end,col-1,'.');
		
		entranceRow = start;
		exitRow = end;
		
	}
	
	/**
	 * Constructor for maze class that sets it up as predefined maze.
	 * @param m The maze.
	 * @param ent The entrance row of the maze.
	 * @param ex The exit row of the maze.
	 */
	public Maze(char[][] m, int ent, int ex) {
		map = m;
		entranceRow = ent;
		exitRow = ex;
	}
	
	/**
	 * Get the current cell's value.
	 * @param r The row.
	 * @param c The column.
	 * @return The value of the maze at the given row and column.
	 */
	public char getCell(int r, int c) {
		return map[r][c];
	}
	
	/**
	 * Set the current cell's value.
	 * @param r The row.
	 * @param c The column.
	 * @param val The value you want to change the cell to.
	 */
	public void setCell(int r, int c, char val) {
		map[r][c] = val;
	}
	
	/**
	 * Get the row the maze starts on.
	 * @return Integer value of the row.
	 */
	public int getEntranceRow() {
		return entranceRow;
	}
	
	/**
	 * Get the row the maze ends on.
	 * @return Integer value of the row.
	 */
	public int getExitRow() {
		return exitRow;
	}
	
	/**
	 * Decides whether or not the given space is open.
	 * @param r The row.
	 * @param c The column.
	 * @return Boolean of whether or not the given space is available.
	 */
	public boolean isOpenSpace(int r, int c) {
		if (map[r][c] == '.') {
			return true;
		} else {
			return false;
		}
	}
	
	/**
	 * Prints the maze out to the user.
	 */
	public void printMaze() {
		for(int i = 0; i < map.length; i++)
		{
		    for(int j = 0; j < map[i].length; j++)
		    {
		        System.out.print(map[i][j]);
		        if(j < map[i].length - 1) System.out.print(" ");
		    }
		    System.out.println();
		}
	}
	
	/**
	 * Returns the last column for a maze.
	 * @return The last column for the maze.
	 */
	public int getLastColumn(int row) {
		return map[row].length-1;
	}
	
	/**
	 * Returns the last row for a given column.
	 * @param col The column.
	 * @return
	 */
	public int getLastRow() {
		return map.length-1;
	}
	
}
