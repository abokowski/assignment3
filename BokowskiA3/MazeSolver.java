/**
 * Class MazeSolver does the recursive solving of the given maze.
 * 
 * @author Alec Bokowski
 *
 */
public class MazeSolver {
	
	private Maze maze;
	private int steps;
	
	/**
	 * Constructor for class MazeSolver.
	 * @param m The maze.
	 */
	public MazeSolver(Maze m) {
		maze = m;
	}
	
	/**
	 * This method recursively runs an operation trying to solve a given maze.
	 * @param row The row.
	 * @param col The column.
	 * @return Returns true if an end was found or false if no end was found.
	 */
	public boolean solveMaze(int row, int col) {
		
		boolean done = false;
		
		if (maze.isOpenSpace(row, col)) {
	    	  
			//mark this cell as being looked over
	        maze.setCell(row, col, 'x');
	        steps += 1;
	        
	        if (row == maze.getExitRow() && col == maze.getLastColumn(row)) {
	        	//maze has been solved, this is the stopping point
	        	done = true;  // maze is solved
	        	
	        } else {
	        	
	        	//check each of the paths this maze can go and if any lead to being solved
	        	
	        	if (!done) {
	        		if (row < maze.getLastRow()) {
		        		//check below
		        		done = solveMaze(row+1, col);  
		        	}
	        	}
	        	
	        	if (!done) {
	        		if (col < maze.getLastColumn(row)) {
	        			//check right
	        			done = solveMaze(row, col+1);  
	        		}
	        	}
	        	
	            if (!done) {
	            	if (row > 0) {
	            		//check above
	            		done = solveMaze(row-1, col);
	            	}
	            }
	            
	            if (!done) {
	            	if (col > 0) {
	            		//check left
	            		done = solveMaze(row, col-1);  
	            	}		
	            }
	            
	            if (!done) {
	            	steps -= 1;
	            	maze.setCell(row,col,'.');
	            }
	            
	         }
	        
	         
		}
	      

		return done;
	}
	
	/**
	 * The number of steps it has taken to solve maze or get to current point.
	 * @return Number of steps taken to solve maze.
	 */
	public int getSteps() {
		return steps;
	}
	
	
}
