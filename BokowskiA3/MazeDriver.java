import java.util.InputMismatchException;
import java.util.Scanner;

/**
 * MazeDriver is the interface for the assignment and handles user interaction.
 * 
 * @author Alec Bokowski
 *
 */
public class MazeDriver {
	
	//maze one stuff
	char[][] m1 = {{'#','#','#','#','#','#','#','#','#','#','#','#'},
			 {'#','.','.','.','#','.','.','.','.','.','.','#'},
			 {'.','.','#','.','#','.','#','#','#','#','.','#'},
			 {'#','#','#','.','#','.','.','.','.','#','.','#'},
			 {'#','.','.','.','.','#','#','#','.','#','.','.'},
			 {'#','#','#','#','.','#','.','#','.','#','.','#'},
			 {'#','.','.','#','.','#','.','#','.','#','.','#'},
			 {'#','#','.','#','.','#','.','#','.','#','.','#'},
			 {'#','.','.','.','.','.','.','.','.','#','.','#'},
			 {'#','#','#','#','#','#','.','#','#','#','.','#'},
			 {'#','.','.','.','.','.','.','#','.','.','.','#'},
			 {'#','#','#','#','#','#','#','#','#','#','#','#'}};
	Maze mazeOne = new Maze(m1, 2, 4);
	MazeSolver maze1 = new MazeSolver(mazeOne);
	
	//maze two stuff
	char[][] m2 = {{'#','#','#','#','#','#','#','#','#','#','#','#'},
			 {'#','.','.','.','#','.','.','.','#','#','.','.'},
			 {'#','.','#','.','.','.','#','.','.','.','.','#'},
			 {'#','.','#','#','#','#','.','#','.','#','.','#'},
			 {'#','.','.','.','#','#','.','.','.','.','.','#'},
			 {'#','#','#','.','#','#','#','#','.','#','.','#'},
			 {'.','.','.','.','.','.','.','.','.','.','#','#'},
			 {'#','#','#','#','#','#','#','#','#','#','#','#'}};
	Maze mazeTwo = new Maze(m2, 6, 1);
	MazeSolver maze2 = new MazeSolver(mazeTwo);
	
	//maze three stuff
	char[][] m3 = {{'#','#','#','#','#','#','#','#','#'},
			 {'#','.','#','.','#','.','.','.','#'},
			 {'#','.','.','.','#','.','#','#','#'},
			 {'#','#','#','.','#','.','#','.','.'},
			 {'.','.','.','.','.','.','#','.','#'},
			 {'#','#','.','#','.','#','#','.','#'},
			 {'#','.','.','#','.','#','.','.','#'},
			 {'#','#','.','#','.','#','.','.','#'},
			 {'#','#','#','#','#','#','#','#','#'}};
	Maze mazeThree = new Maze(m3, 4, 3);
	MazeSolver maze3 = new MazeSolver(mazeThree);
	
	Maze mazeFour = new Maze();
	MazeSolver maze4 = new MazeSolver(mazeFour);
	
	//scanner for user input
	private Scanner input;
	
	//random maze stuff
	
	/**
	 * Starting point of the program.
	 * @param args Dont touch this!
	 */
	public static void main(String[] args) {
		
		MazeDriver driver = new MazeDriver();
		
	}
	
	/**
	 * Constructor for class MazeDriver.
	 */
	public MazeDriver() {
		
		input = new Scanner(System.in);
		int answer = 0;
		
		//introduction message and choices
		System.out.println("Which Maze would you like?");
		System.out.println("1. Maze 1");
		System.out.println("2. Maze 2");
		System.out.println("3. Maze 3");
		System.out.println("4. Random");
		
		System.out.print("Enter choice: ");
		try {
			answer = input.nextInt();
			
			while (answer < 1 || answer > 4) {
				System.out.print("Not a valid answer, please enter a number 1 through 4: ");
				answer = input.nextInt();
			}
		} catch (Exception e) {
			System.out.println("Number was formatted wrong, please restart the program");
		}
		
		//if user chooses first maze
		if (answer == 1) {
			System.out.println("\nShowing maze 1: \n");
			
			mazeOne.printMaze();
			
			//try to solve the maze
			boolean solved = maze1.solveMaze(2, 0);
			
			if (solved) {
				System.out.println("\nSolved your maze: \n");
	            mazeOne.printMaze();
	            System.out.printf("\nPath took %d steps", maze1.getSteps());
			} else {
				System.out.println("\nNot solved");
				mazeOne.printMaze();
			}
			
		}
		
		//if user chooses second maze
		if (answer == 2) {
			System.out.println("\nShowing maze 2: \n");
			
			mazeTwo.printMaze();
			
			//try to solve the maze
			boolean solved = maze2.solveMaze(6, 0);
			if (solved) {
				System.out.println("\nSolved your maze: \n");
	            mazeTwo.printMaze();
	            System.out.printf("\nPath took %d steps", maze2.getSteps());
			} else {	
				System.out.println("\nNot solved");
			}
		}
		
		//if user chooses third maze
		if (answer == 3) {
			System.out.println("\nShowing maze 3: \n");
			
			mazeThree.printMaze();
			
			//try to solve the maze
			boolean solved = maze3.solveMaze(4, 0);
			if (solved) {
				System.out.println("\nSolved your maze: \n");
	            mazeThree.printMaze();
	            System.out.printf("\nPath took %d steps", maze3.getSteps());
			} else {
				System.out.println("\nNot solved");
			}
		}
		
		//if user chooses random maze
		if (answer == 4) {
			System.out.println("\nShowing random maze: \n");
			
			mazeFour.printMaze();
			
			//try to solve the maze
			boolean solved = maze4.solveMaze(mazeFour.getEntranceRow(), 0);
			if (solved) {
				System.out.println("\nSolved your maze: \n");
	            mazeFour.printMaze();
	            System.out.printf("\nPath took %d steps", maze4.getSteps());
			} else {
				System.out.println("\nNot solved");
			}
			
		}
		
	}

}
